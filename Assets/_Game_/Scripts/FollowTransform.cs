using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour {


    public Transform Target;
    public float MovingSpeed = 1f;
    public Vector2 Offset = Vector2.zero;

	void Update ()
    {
        if(Target)
        {
            Vector2 pos = transform.position;
            pos = Vector2.MoveTowards(pos, (Vector2)Target.position + Offset, MovingSpeed * Time.deltaTime);
            MoveTo(pos);
        }            
	}

    public void MoveTo(Vector2 position)
    {
        float savedZ = transform.position.z;
        transform.position = new Vector3(position.x, position.y, savedZ);
    }
}
