using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [System.Serializable]
    public class SoundData
    {
        public AudioClip created;
        public AudioClip destroyed;
    }
    public SoundData Sounds = new SoundData();

    private void OnEnable()
    {
        GameManager.Instance.PlaySoundFx(Sounds.created);
    }

    private void OnDisable()
    {
        GameManager.Instance.PlaySoundFx(Sounds.destroyed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!collision.CompareTag("Player"))
            Destroy(this.gameObject, 0.1f);
        Zombie zombieBhv = collision.GetComponent<Zombie>();
        if(zombieBhv)
        {
            Rigidbody2D rigidBody2D = GetComponent<Rigidbody2D>();
            zombieBhv.Hit(1);
            zombieBhv.RigidBody.AddForce(rigidBody2D.velocity, ForceMode2D.Impulse); 
        }
        else
        {
            Wall wallBhv = collision.GetComponent<Wall>();
            if (wallBhv)
                wallBhv.Hit(1);
        }
    }
}
