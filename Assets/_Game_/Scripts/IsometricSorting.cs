using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsometricSorting : MonoBehaviour {

    public float PixelsPerUnit = 32;

    private SpriteRenderer m_spriteRenderer;
    void Start()
    {
        m_spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update ()
    {
        float posY = transform.position.y;        
        int sortingOrder = -Mathf.FloorToInt(posY * PixelsPerUnit);
        m_spriteRenderer.sortingOrder = sortingOrder;
	}
}
