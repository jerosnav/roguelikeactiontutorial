using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class GameManager : MonoBehaviour, ISerializationCallbackReceiver {

    [Header("UI")]
    public Text BulletText;
    public Text HealthText;
    public Text LevelText;
    public CanvasGroup LevelIntro;
    [Header("Game")]
    public Object[] Levels;
    [Header("Audio")]
    public AudioSource SoundFxAudioSource;

    [SerializeField, HideInInspector]
    private string[] m_levelNames;
    private int m_currentLevelIndex = 0;

    #region ISerializationCallbackReceiver
    public void OnBeforeSerialize()
    {
        if (Application.isEditor)
        {
            //clean null scenes
            Levels = Levels.Where(x => x != null).ToArray();
            m_levelNames = new string[Levels.Length];
            for (int i = 0; i < Levels.Length; ++i)
            {
                m_levelNames[i] = Levels[i].name;
            }
        }
    }

    public void OnAfterDeserialize()
    {

    }
    #endregion

    public static GameManager Instance { get { return s_instance; } }
    static GameManager s_instance = null;
    private void Awake()
    {
        if (!s_instance)
        {
            s_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        LevelIntro.alpha = 1f;
        LevelText.text = SceneManager.GetActiveScene().name;
        StartCoroutine(LevelIntroCO(false));
    }

    public void GameOver()
    {
        LevelText.text = "Game Over\n\nYou survived until " + SceneManager.GetActiveScene().name;
        m_currentLevelIndex = 0;
        StartCoroutine(LevelIntroCO(true));
    }

    public void Win()
    {
        LevelText.text = "You Win!!!\n\nYou survived until " + SceneManager.GetActiveScene().name;
        m_currentLevelIndex = 0;
        StartCoroutine(LevelIntroCO(true));
    }

    public void GoToNextLevel()
    {
        if (m_currentLevelIndex < m_levelNames.Length - 1)
        {
            LevelText.text = m_levelNames[++m_currentLevelIndex];
            StartCoroutine(LevelIntroCO(false));
        }
        else
        {
            Win();
        }
    }

    public void UpdateBulletText()
    {
        BulletText.text = "x" + Player.Instance.Bullets;
    }

    public void UpdateHealthText()
    {
        HealthText.text = "x" + Player.Instance.Health;
    }

    public void PlaySoundFx(AudioClip clip, float volumeScale = 1f)
    {
        if (clip)
        {
            SoundFxAudioSource.pitch = Random.Range(0.9f, 1.1f);
            SoundFxAudioSource.PlayOneShot(clip, volumeScale);
        }
    }

    public void PlaySoundFx(AudioClip[] clips, float volumeScale = 1f)
    {
        if (clips != null && clips.Length > 0)
        {
            AudioClip clip = clips[Random.Range(0, clips.Length)];
            PlaySoundFx(clip, volumeScale);
        }
    }

    private IEnumerator LevelIntroCO(bool isGameOver)
    {
        Time.timeScale = 0f;
        yield return LevelIntroFadeIn();
        if (isGameOver)
        {
            yield return new WaitForSecondsRealtime(4f);
            Destroy(Player.Instance.gameObject);
            LevelText.text = m_levelNames[0];
            yield return new WaitForSecondsRealtime(2f);
            SceneManager.LoadScene(m_levelNames[m_currentLevelIndex]);
        }
        else
        {            
            yield return new WaitForSecondsRealtime(2f);
            SceneManager.LoadScene(m_levelNames[m_currentLevelIndex]);
        }
        yield return LevelIntroFadeOut();
        Time.timeScale = 1f;
    }

    private IEnumerator LevelIntroFadeIn()
    {
        while(LevelIntro.alpha < 1f)
        {
            LevelIntro.alpha += Time.unscaledDeltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    private IEnumerator LevelIntroFadeOut()
    {
        while (LevelIntro.alpha > 0f)
        {
            LevelIntro.alpha -= Time.unscaledDeltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }
}
