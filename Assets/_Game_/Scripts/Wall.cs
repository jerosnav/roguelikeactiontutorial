using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Wall : MonoBehaviour {

    public Sprite DamagedSprite;
    public int Health = 3;

    private SpriteRenderer m_spriteRenderer;
    private void Start()
    {
        m_spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        //Snap to Grid (Editor only)
        if (!Application.isPlaying)
        {
            Vector2 pos = transform.position;
            pos.x = Mathf.Floor(pos.x) + .5f;
            pos.y = Mathf.Floor(pos.y) + .5f;
            transform.position = pos;
        }
    }

    public void Hit(int damage)
    {
        Health -= damage;
        if(Health > 0)
        {
            m_spriteRenderer.sprite = DamagedSprite;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
