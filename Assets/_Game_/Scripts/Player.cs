using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float MovingForce = 1f;
    public int Health = 100;
    public int Bullets = 10;
    [System.Serializable]
    public class SoundData
    {
        public AudioClip[] footSteps;
        public AudioClip hurt;
        public AudioClip dead;
    }
    public SoundData Sounds = new SoundData();
    [Header("Bullet")]
    public Rigidbody2D BulletPrefab;
    public float BulletSpeed = 5f;
    public float BulletsPerSecond = 3f;

    private Vector2 m_shootDir = Vector2.right;
    private float m_shootTimer = 0f;

    public static Player Instance { get { return s_instance; } }
    protected Rigidbody2D m_rigidBody;
    protected Animator m_animator;
    protected SpriteRenderer m_spriteRenderer;

    private static Player s_instance;
    private void Awake()
    {
        if (!s_instance)
        {
            s_instance = this;
            DontDestroyOnLoad(gameObject);                        
        }
        else
        {
            s_instance.transform.position = this.transform.position;
            Destroy(this.gameObject);
        }

        //Move Camera to Player position
        FollowTransform camFollowBhv = Camera.main.GetComponent<FollowTransform>();
        camFollowBhv.Target = s_instance.transform;
        camFollowBhv.MoveTo(s_instance.transform.position);
    }
    void Start ()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        m_spriteRenderer = GetComponent<SpriteRenderer>();
        GameManager.Instance.UpdateBulletText();
        GameManager.Instance.UpdateHealthText();
    }

    private float m_footStepSoundTimer;
    protected Vector2 m_movingVector;
	protected virtual void Update ()
    {
        if (Time.timeScale == 0)
            return;
        bool isAttacking = Input.GetButtonDown("Fire1");
        float hAxis = Input.GetAxisRaw("Horizontal");
        float vAxis = Input.GetAxisRaw("Vertical");

        m_movingVector = new Vector2(hAxis, vAxis).normalized;

        if ( Mathf.Abs(m_movingVector.x) > 0.1f )
            m_spriteRenderer.flipX = m_movingVector.x < 0f;

        bool isWalking = m_movingVector.sqrMagnitude > 0.01f;
        m_animator.SetBool("isWalking", isWalking);
        if(isWalking)
        {
            m_shootDir = m_rigidBody.velocity.normalized;
            m_footStepSoundTimer -= Time.deltaTime;
            if (m_footStepSoundTimer <= 0)
            {
                m_footStepSoundTimer += 0.3f;
                GameManager.Instance.PlaySoundFx(Sounds.footSteps, .5f);
            }
        }

        if (m_shootTimer > 0f)
            m_shootTimer -= Time.deltaTime;

        if (isAttacking && m_shootTimer <= 0f)
        {
            if(Bullets > 0)
                --Bullets;
            GameManager.Instance.UpdateBulletText();

            //Mouse Aiming
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 mousePos = Camera.main.ScreenPointToRay(Input.mousePosition).GetPoint(0);
                m_shootDir = (mousePos - (Vector2)transform.position).normalized;
            }
            m_shootTimer = 1f / BulletsPerSecond;
            m_animator.SetTrigger("attack");
            Rigidbody2D bulletRigidBody = Instantiate(BulletPrefab, transform.position + Vector3.up * 0.5f, Quaternion.identity);
            bulletRigidBody.velocity = m_shootDir * BulletSpeed;
            bulletRigidBody.transform.localScale = new Vector3(1f, -1f, 1f);
            // No bullets? use melee atack (hiddig the bullet and reducing the time to live)
            bulletRigidBody.GetComponent<SpriteRenderer>().enabled = Bullets > 0;
            Destroy(bulletRigidBody.gameObject, Bullets > 0? 5f : 0.2f);
        }
    }

    protected virtual void FixedUpdate()
    {        
        m_rigidBody.AddForce(MovingForce * m_movingVector * m_rigidBody.mass);
    }

    public void Hit(int damage)
    {
        m_animator.SetTrigger("hurt");
        Health -= damage;
        GameManager.Instance.UpdateHealthText();
        if (Health <= 0)
        {
            GameManager.Instance.SoundFxAudioSource.Stop();
            GameManager.Instance.PlaySoundFx(Sounds.dead);
            GameManager.Instance.GameOver();
        }
        else
        {
            GameManager.Instance.PlaySoundFx(Sounds.hurt);
        }
    }
}
