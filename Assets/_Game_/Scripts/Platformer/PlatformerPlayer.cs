using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerPlayer : Player {

    [Header("Platformer")]
    public float JumpingSpeed = 5f;
    public float AirborneForce = 5f;
    [System.Serializable]
    public class RayData
    {
        public Vector2 position;
        public Vector2 direction;
    }
    public RayData[] GroundedRayCheck;
    public LayerMask GroundLayers;

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        if (GroundedRayCheck != null)
        {
            foreach (RayData ray in GroundedRayCheck)
            {
                Vector2 pos = ray.position + (Vector2)transform.position;
                Gizmos.DrawLine(pos, pos + ray.direction);
            }
        }
    }

    protected override void Update()
    {
        base.Update();
        m_movingVector.y = 0f; //reset up/down movement

        if(Input.GetButtonDown("Jump"))
        {
            m_rigidBody.velocity = Vector2.up * JumpingSpeed;
        }

        //m_spriteRenderer.color = IsGrounded() ? Color.white : Color.red;
    }

    protected override void FixedUpdate()
    {
        float force = IsGrounded() ? MovingForce : AirborneForce;
        m_rigidBody.AddForce(force * m_movingVector * m_rigidBody.mass);
    }

    public bool IsGrounded()
    {
        if (GroundedRayCheck != null)
        {
            foreach (RayData ray in GroundedRayCheck)
            {
                Vector2 pos = ray.position + (Vector2)transform.position;
                RaycastHit2D hit = Physics2D.Raycast(pos, ray.direction, ray.direction.magnitude, GroundLayers);
                if(hit)
                {
                    return true;
                }
            }
        }
        return false;
    }

}
