using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ParallaxBackground : MonoBehaviour {

    public Vector2 ParallaxFactor = new Vector2(1f, 1f);
    public Vector2 Offset = Vector2.zero;
    public bool UseMainCameraPosition = true;

    void OnEnable()
    {
        Camera.onPreCull += DoOnPreCull;
    }

    void OnDisable()
    {
        Camera.onPreCull -= DoOnPreCull;
    }

    private void DoOnPreCull(Camera cam)
    {
        Vector2 pos2D = UseMainCameraPosition? Camera.main.transform.position : cam.transform.position;
        pos2D = Vector2.Scale(pos2D + Offset, ParallaxFactor);
        transform.position = new Vector3(pos2D.x, pos2D.y, transform.position.z);
    }
}
