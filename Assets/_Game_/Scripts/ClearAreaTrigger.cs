using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ClearAreaTrigger : MonoBehaviour {

    public List<GameObject> Enemies = new List<GameObject>();
    public UnityEvent TriggerEvent;

    void Start()
    {
        Zombie[] zombies = FindObjectsOfType<Zombie>();
        foreach (Zombie z in zombies)
            Enemies.Add(z.gameObject);
    }

    private bool m_activated = false;
	void Update () 
    {
	    if(!m_activated)
        {
            bool isAllDead = true;
            foreach(GameObject go in Enemies)
            {
                if(go)
                {
                    isAllDead = false;
                    break;
                }
            }

            if (isAllDead)
            {
                m_activated = true;
                if(TriggerEvent != null)
                    TriggerEvent.Invoke();
            }
        }
	}
}
