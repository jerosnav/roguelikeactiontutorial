using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {

	public enum eCollectibleType
    {
        Bullets,
        Food,
    }
    public eCollectibleType Type = eCollectibleType.Food;
    public int Quantity = 1;
    [System.Serializable]
    public class SoundData
    {
        public AudioClip[] collect;
    }
    public SoundData Sounds = new SoundData();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameManager.Instance.PlaySoundFx(Sounds.collect, 1.5f);
        Destroy(gameObject);
        switch(Type)
        {
            case eCollectibleType.Food:
                Player.Instance.Health += Quantity;
                GameManager.Instance.UpdateHealthText();
                break;
            case eCollectibleType.Bullets:
                Player.Instance.Bullets += Quantity;
                GameManager.Instance.UpdateBulletText();
                break;
        }
    }
}
