using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Zombie : MonoBehaviour
{
    public float MovingForce = 20f;
    public float AttackCooldown = 1f;
    public int AttackDamage = 5;
    public int Health = 5;
    public Image LifeBar;
    public Animator DeadFx;
    [System.Serializable]
    public class SoundData
    {
        public AudioClip attack;
        public AudioClip hurt;
        public AudioClip dead;
    }
    public SoundData Sounds = new SoundData();
    [Header("AI Parameters")]
    public Vector2 RandomMovingTime = new Vector2(1f, 4f);
    public Vector2 WaitingTime = new Vector2(1f, 6f);
    public float SightDistance = 1f;
    public LayerMask BlockingLayers;

    public Rigidbody2D RigidBody { get { return m_rigidBody; } }

    private Vector2 m_movingForce;    

    private Rigidbody2D m_rigidBody;
    private SpriteRenderer m_spriteRenderer;
    private Animator m_animator;
    private int m_maxHealth;
    private float m_lifebarTimer = 0f;
    private void Start()
    {
        m_maxHealth = Health;
        m_rigidBody = GetComponent<Rigidbody2D>();
        m_spriteRenderer = GetComponent<SpriteRenderer>();
        m_animator = GetComponent<Animator>();
        StartCoroutine(AIWalkRandomly());
        StartCoroutine(AILookForPlayer());
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        UnityEditor.Handles.color = Color.green;
        UnityEditor.Handles.CircleHandleCap(0, transform.position, Quaternion.identity, SightDistance, EventType.Repaint);
        UnityEditor.Handles.color = Color.white;
    }
#endif

    private void Update()
    {
        if (m_lifebarTimer > 0f)
            m_lifebarTimer -= Time.deltaTime;
        LifeBar.enabled = m_lifebarTimer > 0f;
        LifeBar.fillAmount = (float)Health / m_maxHealth;
        LifeBar.color = Color.Lerp(Color.red, Color.green, LifeBar.fillAmount);
        if( Mathf.Abs(m_movingForce.x) > 0.1f )
            m_spriteRenderer.flipX = m_movingForce.x < 0f;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.transform.gameObject.layer == 0)
            m_movingForce = Vector2.Reflect(m_movingForce, collision.contacts[0].normal);
    }

    private float m_timeForNextAttack;
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (Time.time >= m_timeForNextAttack && collision.gameObject.CompareTag("Player"))
        {
            m_timeForNextAttack = Time.time + AttackCooldown;
            m_animator.SetTrigger("attack");
            GameManager.Instance.PlaySoundFx(Sounds.attack);
            Player.Instance.Hit(AttackDamage);
        }
    }

    private void FixedUpdate()
    {
        m_rigidBody.AddForce(m_movingForce * m_rigidBody.mass);
    }

    public void Hit(int damage)
    {
        Health -= damage;
        m_lifebarTimer = 2f;
        m_animator.SetTrigger("hurt");
        if(Health <= 0f)
        {
            GameManager.Instance.PlaySoundFx(Sounds.dead);
            Destroy(this.gameObject);
            if(DeadFx)
            {
                Animator fxAnimator = Instantiate(DeadFx, transform.position + Vector3.up / 2f, Quaternion.identity);
                Destroy(fxAnimator.gameObject, fxAnimator.GetCurrentAnimatorStateInfo(0).length);
            }
        }
        else
        {
            GameManager.Instance.PlaySoundFx(Sounds.hurt);
        }
    }

    private IEnumerator AILookForPlayer()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            if (Player.Instance)
            {
                Vector2 dir = Player.Instance.transform.position - transform.position;
                RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, dir, SightDistance, BlockingLayers);
                if (hitInfo.transform == Player.Instance.transform)
                {
                    StopAllCoroutines();
                    StartCoroutine(AIFollowPlayer());
                }
            }
        }
    }

    private IEnumerator AIWalkRandomly()
    {
        while (true)
        {
            m_animator.SetBool("isWalking", false);
            m_movingForce = Vector2.zero;
            yield return new WaitForSeconds(Random.Range(WaitingTime.x, WaitingTime.y));
            m_animator.SetBool("isWalking", true);
            Vector2 randDir = Random.rotationUniform * Vector2.up;
            m_movingForce = randDir * MovingForce;
            yield return new WaitForSeconds(Random.Range(RandomMovingTime.x, RandomMovingTime.y));
        }
    }

    private bool m_isFollowingPlayer = false;
    private IEnumerator AIFollowPlayer()
    {
        m_animator.SetBool("isWalking", true);
        m_isFollowingPlayer = true;
        while (m_isFollowingPlayer && Player.Instance)
        {
            Vector2 dir = Player.Instance.transform.position - transform.position;
            m_movingForce = dir.normalized * MovingForce;
            yield return null;
            m_isFollowingPlayer = dir.magnitude < 2f * SightDistance;
        }
        StopAllCoroutines();
        StartCoroutine(AIWalkRandomly());
        StartCoroutine(AILookForPlayer());
    }
}
